// Model View script using Knockout
"use strict";

ko.options.deferUpdates = true;

var SimpleListModel = function () {
  self = this;

  // save currently bouncing marker (to be stopped when other marker is animating)
  self.bouncing_marker = null;

  // this is a special array that holds "filtered"
  // which changes according to the terms.
  // this array is being displayed to the user.
  self.filtered_items = ko.observableArray();

  // this.items holds an array of place and marker, returned by Google Places
  self.items = [];

  // filter search term
  self.search_term = ko.observable("");
  self.search = function () {

    // hide all display window
    hideInfoWindow();

    // stop any bouncing marker
    stop_marker_animation(self.bouncing_marker);
    self.bouncing_marker = null;


    var term = self.search_term();
    console.log('search by ', term);
    self.filtered_items.removeAll();
    // for (var i in self.items) {
    //   var item = self.items[i];

    self.items.forEach(function (item) {

      var name = item.place.name;
      // case-insensitive regex search ("i")
      var re = new RegExp(term, "i");
      if (name == undefined || name == "" || re.test(name)) {
        self.filtered_items.push(item);
        item.marker.setVisible(true);
        console.log(name);
      }
      else {
        item.marker.setVisible(false);
      }
    });
  }.bind(self);

  self.search_term.subscribe(function (term) {
    console.log(term);
    self.search_term(term);
    self.search();
  });

  self.onClick = function () {
    // hide all display window
    hideInfoWindow();

    // stop all other animating markers if any
    stop_marker_animation(self.bouncing_marker);
    self.bouncing_marker = null;

    // save the current marker being animated
    self.bouncing_marker = this.marker;
    this.marker.setAnimation(google.maps.Animation.BOUNCE);

    // simulate marker click event
    google.maps.event.trigger(this.marker, 'click');
  };
};

var viewModel = new SimpleListModel();
ko.applyBindings(viewModel);
