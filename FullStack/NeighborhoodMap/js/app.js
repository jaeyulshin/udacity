// contains Google Map and Yelp API
// by Jae Shin
"use strict";

var map;
var infowindow;

function onGoogleMapLoadError() {
  alert('Google Map loading error has occurred. Please try again later.')
}

function onKnockoutLoadError() {
  alert('Knockout Javascript loading error has occurred. Please try again later.')
}

function hideInfoWindow() {
  infowindow.close();
}

function stop_marker_animation(marker) {
  if (!marker) return;
  marker.setAnimation(null);
}

function initMap() {

  const initial_location = {
    lat: 33.4241541,
    lng: -111.9421961
  };
  map = new google.maps.Map(document.getElementById('map'), {
    center: initial_location,
    zoom: 17,
    // styles: styles,
    // mapTypeControl: false
  });

  // list initial restaurants when map loads
  infowindow = new google.maps.InfoWindow();
  var service = new google.maps.places.PlacesService(map);
  service.nearbySearch({
    location: initial_location,
    radius: 200,
    type: ['restaurant']
  }, callback);

  function callback(results, status) {
    // callback is called from above places API for initial searches.
    // get the View Model Context (controller) from knockout library
    // var viewModel = ko.contextFor(window.document.body).$root;
    // viewModel is not a global variable that is an instance.

    if (status === google.maps.places.PlacesServiceStatus.OK) {

      // assign Google Places ALL results
      for (var i = 0; i < results.length; i++) {
        viewModel.items.push({
          place: results[i],
          marker: createMarker(results[i])
        });
      }

      // pass empty string '', to display ALL results
      // initially.
      viewModel.search_term('');
      viewModel.search();
    }
    else {
      alert('Google Maps API returned an error: Please try refresh again.');
    }
  }

  async function get_yelp_info(place) {

    const data = {
      'latitude': place.geometry.location.lat(),
      'longitude': place.geometry.location.lng(),
      'term': place.name
    };

    const accessToken = 'ZrwscDPUN3umkUaEsMq6I30PaG_JFrRWgKaIewRO3hESKKTvzrcVC0qf0NcuTDcDxVuITHLaYlUMz7hHPQRTJXuCEXDd35hbh0943rIxRwnlMUbRWsq8HWHmq_sUXHYx';

    // get additional information using Yelp API
    var result;
    try {
      result = await $.ajax({
        // url: 'https://api.yelp.com/v3/businesses/search',

        // Use https://cors-anywhere.herokuapp.com for proxy
        // For production, this needs to changed to use own server
        url: 'https://cors-anywhere.herokuapp.com/https://api.yelp.com/v3/businesses/search',

        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', 'Bearer ' + accessToken);

          // X-Requested-With is needed to enable CORS with https://cors-anywhere.herokuapp.com/
          xhr.setRequestHeader('X-Requested-With', 'Accept');
        },
        method: 'GET',
        dataType: 'json',
        data
      });
      return result;
    }
    catch (error) {
      console.log('error', error);
      // alert(error.responseText);
    }
  }

  async function displayInfoWindow(place, marker) {
    // var more_info = viewModel.info_window_content(place);

    let full_info = place.name;
    try {
      let yelp_info_list = await get_yelp_info(place);
      const yelp_info = yelp_info_list.businesses[0];
      const rating = yelp_info.rating;
      const price = yelp_info.price; // $, $$, $$$, etc.
      console.log('yelp_info', yelp_info);

      full_info += '<br>';
      if (yelp_info.price) {
        full_info += '<br/>Price: ' + yelp_info.price;
      }

      if (yelp_info.rating) {
        full_info += '<br/>Rating: ' + yelp_info.rating;
      }
    }
    catch (error) {
      console.log('yelp error', error);
      // $('<div></div>').html('Yelp API error: ' + error).dialog();
      full_info += ' (Yelp API Error - Not shown)';
      alert('Yelp API could not be loaded');
    }
    console.log('displayInfoWindow', full_info);
    infowindow.setContent(full_info);
    infowindow.open(map, marker);
  }

  function createMarker(place) {
    const marker = new google.maps.Marker({
      map,
      position: place.geometry.location
    });

    google.maps.event.addListener(marker, 'click', function () {
      console.log('google map marker clicked');
      displayInfoWindow(place, this);
    });

    return marker;
  }
}
