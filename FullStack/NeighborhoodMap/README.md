# Neighborhood Map
This project displays restaurants near Arizona State University,
and allows users to filter the list by typing any portion of the name.

# How to run
Due to Yelp's CORS limitation (unable to get it work using localhost) from localhost origin,
cors-anywhere via heroku is used to route the request (suggested by Udacity reviewer).
The response can be a bit slower.

So, for production, real proxy is needed using own server.

- Run http server on the project directory using a special http server with CORS.
  - python3 -m http.server
  - Open the browser and type http://localhost:8000

# Resources used
- Python3 (for CORS-enabling http server)
- KnockoutJS Framework
- jQuery
- Google Maps Javascript API
- CSS Grid System (Responsive)
- Yelp API via https://cors-anywhere.herokuapp.com
- git repo at https://bitbucket.org/jaeyulshin/udacity/src
  and in this directory: /FullStack/NeighborhoodMap/

