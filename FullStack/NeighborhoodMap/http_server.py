#!/usr/bin/env python3
"""
This is a special http server in python3 that adds CORS header for every request.

This should be used for debug/testing purpose only, and not designed for production level.

Reference: https://stackoverflow.com/questions/21956683/enable-access-control-on-simple-http-server
"""
# encoding: utf-8
"""Use instead of `python3 -m http.server` when you need CORS"""

from http.server import HTTPServer, SimpleHTTPRequestHandler


class CORSRequestHandler(SimpleHTTPRequestHandler):
    def end_headers(self):
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'GET, POST')
        self.send_header('Cache-Control', 'no-store, no-cache, must-revalidate')
        return super(CORSRequestHandler, self).end_headers()


httpd = HTTPServer(('localhost', 8000), CORSRequestHandler)
httpd.serve_forever()