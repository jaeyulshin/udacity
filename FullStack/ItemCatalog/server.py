#!/usr/bin/env python3
"""
Catalog server with Google sign-in with database backend.

- Notes
  - oauth2client is deprecated.  Hence google-auth module is used.

- dependencies
  pip install flask sqlalchemy passlib google-auth

@version: 1.1

@history:

- 1.1: add JSON endpoints. add support for postgresql (for next project)
       add user info associated with each category and item
- 1.0: initial submit

@author: Jae Shin
"""

import os
import json
from random import choice
import string

# SQLalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, scoped_session

# for Google signin authentication
from google.oauth2 import id_token
from google.auth.transport import requests

# Flask lib
from flask import Flask, jsonify, request, redirect, url_for
from flask import render_template, jsonify, flash
from flask import session as login_session

# ORM
from models import Category, Item, Base, db_session

app = Flask(__name__)

# read client id
with open('client_secrets.json', 'r') as f:
    json_content = json.loads(f.read())
    CLIENT_ID = json_content['web']['client_id']

def get_userid():
    """
    Returns Google sign-in userid - this userid is saved to every
    category and item in the database to check for authorization to
    edit/delete
    """

    data = login_session.get('data')
    return data['userid']

def is_authenticated():
    """ Returns TRUE if properly authenticated """

    return login_session.get('authenticated') == True


def is_authorized(userid):
    """ If userid matches currently logged-in user, then authorized """

    # Make sure user is authenticated
    if not is_authenticated():
        return False

    # Authenticated user has 'data' field that has userid, email, name
    data = login_session.get('data')
    if data is None:
        return False

    # Does logged-in user's userid matches the userid parameter?
    return data['userid'] == userid


def make_response(status, data):
    """ Helper function to return http response """
    response = app.response_class(
        response=json.dumps(data), status=status, mimetype='application/json')
    return response


def get_all_categories():
    """ Returns all category list """
    return db_session.query(Category).order_by(Category.name.asc()).all()


def get_all_items(category_id):
    """ Returns all items list under a category """
    return db_session.query(Item).filter(Item.category_id == category_id).all()


def get_item(item_id):
    """ Helper function for returning ORM for Item """
    return db_session.query(Item).filter(Item.id == item_id).first()


def get_category(category_id):
    """ Helper function for returning ORM for Category """
    return db_session.query(Category).filter_by(id=category_id).one()


# Since we're using Google-signin, we don't need a separate login page
# because LOGIN buttons are in every page.
# @app.route('/login')
# def showLogin():
#     state = ''.join(
#         choice(string.ascii_uppercase + string.digits) for x in range(32))
#     login_session['state'] = state
#     # return "The current session state is %s" % login_session['state']
#     return render_template('index.html', STATE=state)


@app.route('/')
@app.route('/catalog')
def get_index():
    """ Main page """
    return render_template('catalog.html', categories=get_all_categories())


@app.route('/item/<int:item_id>')
def show_item_detail(item_id):
    """ View item detail: name and description """

    item = get_item(item_id)
    print(item.name, item.description)
    return render_template(
        'item_detail.html',
        categories=get_all_categories(),
        item=get_item(item_id),
        category_id=item.category_id)


@app.route('/category/<int:category_id>')
def view_category(category_id):
    """ List items under a specific category """

    all_cat = get_all_categories()
    curr_cat = next(x for x in all_cat if x.id == category_id)
    return render_template(
        'items.html',
        categories=all_cat,
        category=curr_cat,
        category_id=curr_cat.id,
        items=get_all_items(category_id))


@app.route('/login')
def get_main():
    """ login page """
    return render_template('login.html')


@app.route('/app_js')
def app_js():
    """ Returns javascript location with 'state' variable """
    return render_template('js/app.js')


@app.route('/google_disconnect')
def google_disconnect():
    if login_session.get('authenticated'):
        del login_session['authenticated']

    if login_session.get('data'):
        del login_session['data']

    return make_response(200, {})


@app.route('/google_tokensignin', methods=['POST'])
def google_tokensignin():
    """ Google sign-in helper. """

    login_session['authenticated'] = False

    try:
        token = request.values.get('id_token')
        print('token received:', token)
        if token is None:
            return make_response(500, {'error': 'empty id token'})

        idinfo = id_token.verify_oauth2_token(token, requests.Request(),
                                              CLIENT_ID)

        print('idinfo', idinfo)
        if idinfo['iss'] not in [
                'accounts.google.com', 'https://accounts.google.com'
        ]:
            raise ValueError('Wrong issuer.')

        # make sure client id matches app's client id
        if idinfo['aud'] != CLIENT_ID:
            raise ValueError('Invalid client id')

        data = {
            'userid': idinfo['sub'],
            'email': idinfo['email'],
            'name': idinfo['name']
        }
        print(data)

        # save to session
        login_session['authenticated'] = True
        login_session['data'] = data

        return make_response(200, data)
    except ValueError as e:
        print(e)
        print('google sign-in error:')
        return make_response(500, {'error': 'google sign-in error'})


@app.route('/category/new/', methods=['GET', 'POST'])
def new_category():
    """Add new category"""

    if not is_authenticated():
        print('Not authenticated. Redirecting to login')
        return redirect(url_for('get_index'))

    if request.method == 'POST':

        # POST method - add a new category
        assert len(request.form['name']) > 0, 'Must be a string'

        category = Category(name=request.form['name'], created_by=get_userid())

        db_session.add(category)
        db_session.commit()
        flash('Category added successfully.')
        return redirect(url_for('view_category', category_id=category.id))
    else:
        return render_template('new_category.html')


@app.route('/category/<int:category_id>/edit', methods=['GET', 'POST'])
def edit_category(category_id):
    """Edit category"""

    if not is_authenticated():
        return redirect('/login')

    category = get_category(category_id)

    if not is_authorized(category.created_by):
        flash('You are not authorized to edit this')
        return redirect(url_for('view_category', category_id=category.id))

    if request.method == 'POST':
        if request.form['name']:
            category.name = request.form['name']

        db_session.add(category)
        db_session.commit()
        flash('Category updated successfully.')
        return redirect(url_for('view_category', category_id=category.id))
    else:
        return render_template('edit_category.html', category=category)


@app.route('/category/<int:category_id>/delete', methods=['GET', 'POST'])
def delete_category(category_id):
    """Delete category"""

    if not is_authenticated():
        return redirect('/login')

    category = get_category(category_id)

    if not is_authorized(category.created_by):
        flash('You are not authorized to delete this')
        return redirect(url_for('view_category', category_id=category.id))

    if request.method == 'POST':
        # delete all items under this category as well
        items = get_all_items(category.id)

        for item in items:
            db_session.delete(item)

        # Now delete category
        cat_name = category.name
        db_session.delete(category)
        db_session.commit()
        flash('Category %s deleted successfully.' % cat_name)
        return redirect(url_for('get_index'))
    else:
        return render_template('delete_category.html', category=category)


@app.route('/item/<int:category_id>/new', methods=['GET', 'POST'])
def new_item(category_id):
    """Add item under a specific category"""

    if not is_authenticated():
        return redirect('/login')

    if request.method == 'POST':
        # POST method - add a new item under this category
        assert len(request.form['name']) > 0, 'Must be a string'
        item = Item(
            name=request.form['name'],
            description=request.form['description'],
            category_id=category_id,
            created_by=get_userid())
        db_session.add(item)
        db_session.commit()

        flash('Item added successfully.')
        return redirect(url_for('view_category', category_id=category_id))

    else:
        return render_template('new_item.html')


@app.route('/item/<int:item_id>/edit', methods=['GET', 'POST'])
def edit_item(item_id):
    """Edit item"""

    if not is_authenticated():
        return redirect('/login')

    item = get_item(item_id)
    if not is_authorized(item.created_by):
        flash('You are not authorized to edit this')
        return redirect(url_for('show_item_detail', item_id=item.id))

    if request.method == 'POST':
        if request.form['name']:
            item.name = request.form['name']

        if request.form['description']:
            item.description = request.form['description']

        db_session.add(item)
        db_session.commit()
        flash('Item updated successfully.')

        return redirect(url_for('show_item_detail', item_id=item.id))
    else:
        return render_template('edit_item.html', item=item)


@app.route('/item/<int:item_id>/delete', methods=['GET', 'POST'])
def delete_item(item_id):
    """ Delete item """

    if not is_authenticated():
        return redirect('/login')

    item = get_item(item_id)
    category_id = item.category_id

    if not is_authorized(item.created_by):
        flash('You are not authorized to delete this')
        return redirect(url_for('show_item_detail', item_id=item.id))

    if request.method == 'POST':
        item_name = item.name
        db_session.delete(item)
        db_session.commit()
        flash('Item %s deleted successfully.' % item_name)
        return redirect(url_for('view_category', category_id=category_id))

    return render_template(
        'delete_item.html', item=item, category_id=category_id)


# JSON endpoints


@app.route('/item/<int:item_id>/json')
def json_get_item(item_id):
    """ Return a single item """

    return jsonify(get_item(item_id).serialize)


@app.route('/category/<int:category_id>/json')
def json_get_items_on_category(category_id):
    """ Returns all items under a category id"""

    items = get_all_items(category_id)

    return jsonify([x.serialize for x in items])


@app.route('/catalog.json')
def catalog_json():

    catalog = {}
    all_cat = []
    catalog['Category'] = all_cat

    for cat in db_session.query(Category).all():
        all_cat.append(cat.serialize)

    for cat in all_cat:
        items = []
        for item in db_session.query(Item).filter(
                Item.category_id == cat['id']).all():
            items.append(item.serialize)
        cat['items'] = items

    return jsonify(catalog)


if __name__ == '__main__':
    app.debug = True

    # flask.session requires secret_key to be assigned
    app.secret_key = 'this_is_secret_key_2018'
    port = os.environ.get('CUSTOM_PORT', 8081)
    app.run(host='0.0.0.0', port=port)
