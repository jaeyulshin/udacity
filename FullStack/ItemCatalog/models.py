"""
This file defines the SQLite database structure for Catalog App.

@author: Jae Shin
"""
import random
import string
from datetime import datetime

from sqlalchemy import ForeignKey, Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, scoped_session
from sqlalchemy import create_engine
from sqlalchemy.pool import SingletonThreadPool
from passlib.apps import custom_app_context as pwd_context
# from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer,
#                           BadSignature, SignatureExpired)

Base = declarative_base()
secret_key = ''.join(
    random.choice(string.ascii_uppercase + string.digits) for x in range(32))


class Category(Base):
    """
    created_by: userid returned by Google signin
    """
    __tablename__ = 'category'

    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    created_by = Column(String(100), nullable=False)

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {'name': self.name, 'id': self.id, 'created_by': self.created_by}


class Item(Base):
    __tablename__ = 'item'

    id = Column(Integer, primary_key=True)
    name = Column(String(80), nullable=False)
    description = Column(String(4096))
    category_id = Column(Integer, ForeignKey('category.id'))
    category = relationship(Category)
    created_on = Column(DateTime, default=datetime.utcnow)
    created_by = Column(String(100), nullable=False)

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'category_id': self.category_id,
            'created_on': self.created_on,
            'created_by': self.created_by
        }


# SQLite
engine = create_engine('sqlite:///catalog.db', convert_unicode=True, poolclass=SingletonThreadPool)
# PostgreSQL
# engine = create_engine('postgresql+psycopg2://catalog_user:123@localhost/catalog')
DBSession = scoped_session(
    sessionmaker(autocommit=False, autoflush=False, bind=engine))

# Base.metadata.bind = engine

db_session = DBSession()
