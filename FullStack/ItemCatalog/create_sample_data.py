#!/usr/bin/env python3
"""
Create a sample data to catalog.db for testing purpose.
"""

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from models import Category, Item, Base, db_session

# Create dummy user
# User1 = User(
#     id=1, name="Jae Shin", email="mastershin@gmail.com", password_hash='x')
# session.add(User1)
# session.commit()

all_category = [(1, 'Baseball'), (2, 'Soccer'), (3, 'Tennis')]

for cat_id, cat_name in all_category:

    # category = Category(id=cat_id, name=cat_name)
    category = Category(name=cat_name, created_by='default')

    db_session.add(category)
    db_session.commit()

all_items = [('Bat', 1, 'Excellent bat'), ('Glove', 1, 'white glove.'),
             ('ball', 1, 'great baseball.')]
all_items.extend([('Soccer ball', 2, 'Great durable soccer ball'),
                  ('Shin Guard', 2, 'Protection for your shin.')])
all_items.extend([('Tennis ball', 3, 'Professional tennis ball'),
                  ('Racquet', 3, 'High quality racquet')])
for item_id, item_data in enumerate(all_items):
    item_name, cat_id, descr = item_data
    # item = Item(
    #    id=item_id + 1, name=item_name, description=descr, category_id=cat_id)
    item = Item(name=item_name, description=descr, category_id=cat_id)
    item.created_by = 'default'
    print(item_id, item_name)

    db_session.add(item)
    db_session.commit()

print("added items!")
