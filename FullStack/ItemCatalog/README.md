# Catalog Apps

This program displays category and items associated with
the category.  In order to modify categories and items
users can sign up with Google account and sign-in.

# Requirement
- Python 3
- See requirements.txt for python libraries

# Getting Started
- Create a database by running python3 database_setup.py
- (optional) Create a sample data by running create_sample_data.py
- For flask server, execute: python3 server.py
- In browser, go to localhost:8081 to access the app
- For JSON endpoints, /catalog.json, /item/{item_id}/json, /category/{category_id}/json

# Version
## v1.0
- Initial version
