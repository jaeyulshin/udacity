function onSignIn(googleUser) {
  // Useful debugging data for client-side scripts:
  var profile = googleUser.getBasicProfile();
  console.log("ID: " + profile.getId()); // Don't send this directly to your server!
  console.log('Full Name: ' + profile.getName());
  console.log("Image URL: " + profile.getImageUrl());
  console.log("Email: " + profile.getEmail());

  // The ID token you need to pass to your backend:
  var id_token = googleUser.getAuthResponse().id_token;
  console.log("ID Token: " + id_token);

  $.post('/google_tokensignin?state={{STATE}}', { id_token: id_token },
    function (result) {

      console.log('google tokensignin result', result)
      if (result && result.userid) {
        console.log('Login success')

        // result: {userid: "**numbers**", email: "", name: ""}
        var userid = result.userid
        var email = result.email
        var name = result.name
        console.log(userid, email, name)

        $('.g-signout2').show();
        $('.editable').show();

        // redirect to main page
        //if (window.location.pathname != "/") {
        //window.location.href = "/";
        //}
      }
    }
  );


  // var xhr = new XMLHttpRequest();
  // xhr.open('POST', '/google_tokensignin');
  // xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  // xhr.onload = function () {
  //   console.log('Signed in as: ' + xhr.responseText);
  // };
  // xhr.send('idtoken=' + id_token);
}

function signOut() {

  $.get('/google_disconnect?state={{STATE}}', {},
    function (result) {
      if (result) {
        console.log('server logout', result)
      }
    }
  );

  var auth2 = gapi.auth2.getAuthInstance();
  auth2.signOut().then(function () {
    console.log('User signed out.');
    window.location.href = "/";
  });
}
