#!/usr/bin/env python3
"""
Database setup class.  Creates catalog.db (for SQLite only)

This is NOT needed for PostgreSQL setup. catalog.sql should be loaded.
"""

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine

from passlib.apps import custom_app_context as pwd_context
from models import Base

engine = create_engine('sqlite:///catalog.db')
Base.metadata.create_all(engine)
