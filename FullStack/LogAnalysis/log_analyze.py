#!/usr/bin/env python3
"""
Udacity Log Analysis Project

@version: 0.2

@author: Jae Shin
"""
import datetime
from decimal import Decimal
import psycopg2

conn = psycopg2.connect('host=127.0.0.1 dbname=news')

cursor = conn.cursor()


def print_most_popular_three_articles():
    """
    Prints 3 most popular articles, sorted with the most popular at the top.
    """
    try:

        sql = """
        select title, count(title) from articles
        join log on articles.slug = split_part(log.path, '/', 3)
        group by title
        order by count(title) desc
        limit 3
        """
        cursor.execute(sql)

        records = cursor.fetchall()
        for r in records:
            print('"{}" - {} views'.format(r[0], r[1]))

    except Exception as e:
        print("db error: " + str(e))


def print_most_popular_authors():

    try:

        sql = """
        select authors.name, count(authors.name)
        from authors join articles on authors.id=articles.author
        join log on articles.slug = split_part(log.path, '/', 3)
        group by authors.name
        order by count(authors.name) desc
        """
        cursor.execute(sql)

        records = cursor.fetchall()
        for r in records:
            print('"{}" - {} views'.format(r[0], r[1]))

    except Exception as e:
        print("db error: " + str(e))


def print_where_log_error_exists():
    try:

        sql = """
        select date_trunc('day', time) as day,
        (sum(case when status='404 NOT FOUND' then 1. else 0. end)
          / sum(case when status!='404 NOT FOUND' then 1. else 0. end))
          as error_pct
        from log group by 1
        having (sum(case when status='404 NOT FOUND' then 1. else 0. end)
          / sum(case when status!='404 NOT FOUND' then 1. else 0. end)) >= 0.01
        order by 2 desc;
        """
        cursor.execute(sql)

        records = cursor.fetchall()
        for r in records:
            the_date = datetime.datetime.date(r[0])
            the_date_str = the_date.strftime('%b %d, %Y')
            print('{0} - {1:.2g}% errors'.format(the_date_str,
                                                 r[1] * Decimal('100.')))

    except Exception as e:
        print("db error: " + str(e))


if __name__ == '__main__':

    print('What are the most popular three articles of all time')
    print_most_popular_three_articles()

    print()
    print('Who are the most popular article authors of all time?')
    print_most_popular_authors()

    print()
    print('On which days did more than 1% of requests lead to errors?')
    print_where_log_error_exists()
