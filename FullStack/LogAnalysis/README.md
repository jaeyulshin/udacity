# Log Analyzer
This program does 3 queries and prints the result to the screen.
- What are the most popular three articles of all time?
- Who are the most popular article authors of all time? 
- On which days did more than 1% of requests lead to errors?

# Requirement
PostgreSQL with database name 'news' must exist

# Getting Started
- Load the data from newsdata.sql (not included here)
- Make sure PostgreSQL is up and running
- Use the command psql -d news -f newsdata.sql
- python log_analyze.py

# Version
## To do:
- SQL optimization (for future)

## v0.2
- add env python3 to the code
- PEP8 compliant
- update README.md for database setup instruction

## v0.1
- initial submit